package net.styleru.ikomarov.ipcrepository.serivce;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import net.styleru.ikomarov.ipcrepository.repository.IRepositoryAdapter;

import java.util.HashMap;

/**
 * Created by i_komarov on 30.05.17.
 */

public class RepositoryContainer extends Service {

    private IRepositoryAdapter adapter;

    @Override
    public void onCreate() {
        super.onCreate();
        adapter = new RepositoryAdapter(new HashMap<>());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return adapter.asBinder();
    }
}
