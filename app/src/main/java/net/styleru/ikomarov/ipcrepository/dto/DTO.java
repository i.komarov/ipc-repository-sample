package net.styleru.ikomarov.ipcrepository.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 30.05.17.
 */

public class DTO implements Parcelable {

    @NonNull
    private final String id;

    @NonNull
    private final String name;

    private DTO(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public DTO(@NonNull String id, @NonNull String name) {
        this.id = id;
        this.name = name;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DTO> CREATOR = new Creator<DTO>() {
        @Override
        public DTO createFromParcel(Parcel in) {
            return new DTO(in);
        }

        @Override
        public DTO[] newArray(int size) {
            return new DTO[size];
        }
    };
}
