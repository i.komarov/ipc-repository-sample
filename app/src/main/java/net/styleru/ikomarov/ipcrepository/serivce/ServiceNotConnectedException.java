package net.styleru.ikomarov.ipcrepository.serivce;

/**
 * Created by i_komarov on 30.05.17.
 */

public class ServiceNotConnectedException extends IllegalStateException {

    private static final String MESSAGE = "Service is not connected yet!";

    public ServiceNotConnectedException() {
        super(MESSAGE);
    }
}
