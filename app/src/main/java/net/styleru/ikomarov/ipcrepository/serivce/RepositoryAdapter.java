package net.styleru.ikomarov.ipcrepository.serivce;

import android.os.RemoteException;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.ipcrepository.dto.DTO;
import net.styleru.ikomarov.ipcrepository.repository.IDTOCreateCallback;
import net.styleru.ikomarov.ipcrepository.repository.IDTOGetCallback;
import net.styleru.ikomarov.ipcrepository.repository.IRepositoryAdapter;

import java.util.Map;

/**
 * Created by i_komarov on 30.05.17.
 */

public class RepositoryAdapter extends IRepositoryAdapter.Stub {

    private final Map<String, DTO> storage;

    public RepositoryAdapter(@NonNull Map<String, DTO> storage) {
        this.storage = storage;
    }

    @Override
    public void create(DTO dto, IDTOCreateCallback callback) throws RemoteException {
        storage.put(dto.getId(), dto);
        callback.onDTOCreated(dto);
    }

    @Override
    public void get(String id, IDTOGetCallback callback) throws RemoteException {
        callback.onDTOLoaded(storage.get(id));
    }
}
