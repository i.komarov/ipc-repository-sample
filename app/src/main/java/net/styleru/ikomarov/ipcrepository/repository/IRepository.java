package net.styleru.ikomarov.ipcrepository.repository;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.ipcrepository.dto.DTO;

import io.reactivex.Single;

/**
 * Created by i_komarov on 30.05.17.
 */

public interface IRepository {

    Single<Boolean> create(@NonNull DTO dto);

    Single<DTO> get(@NonNull String id);

    void release();
}
