package net.styleru.ikomarov.ipcrepository.serivce;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.ipcrepository.dto.DTO;
import net.styleru.ikomarov.ipcrepository.repository.IDTOCreateCallback;
import net.styleru.ikomarov.ipcrepository.repository.IDTOGetCallback;
import net.styleru.ikomarov.ipcrepository.repository.IRepository;
import net.styleru.ikomarov.ipcrepository.repository.IRepositoryAdapter;

import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;

/**
 * Created by i_komarov on 30.05.17.
 */

public class RepositoryProxy implements IRepository {

    @NonNull
    private final Context context;

    @NonNull
    private IRepositoryAdapter adapter;

    private volatile boolean isConnected;

    private volatile boolean isConnecting;

    private volatile boolean isShuttingDownManually;

    @NonNull
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            isConnecting = false;
            adapter = IRepositoryAdapter.Stub.asInterface(service);
            isConnected = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isConnecting = false;
            isConnected = false;
            if(!isShuttingDownManually) {
                establishConnection();
            }
        }
    };

    public RepositoryProxy(@NonNull Context context) {
        this.context = context.getApplicationContext();
        this.establishConnection();
    }

    @Override
    public Single<Boolean> create(@NonNull DTO dto) {
        return Single.create(emitter -> {
            delaySubscription(emitter);

            if(!isConnected) {
                if(!emitter.isDisposed()) {
                    emitter.onError(new ServiceNotConnectedException());
                }
            } else {
                adapter.create(dto, new IDTOCreateCallback.Stub() {
                    @Override
                    public void onDTOCreated(DTO dto) throws RemoteException {
                        if(!emitter.isDisposed()) {
                            emitter.onSuccess(true);
                        }
                    }
                });
            }
        });
    }

    @Override
    public Single<DTO> get(@NonNull String id) {
        return Single.create(emitter -> {
            delaySubscription(emitter);

            if(!isConnected) {
                if(!emitter.isDisposed()) {
                    emitter.onError(new ServiceNotConnectedException());
                }
            } else {
                adapter.get(id, new IDTOGetCallback.Stub() {
                    @Override
                    public void onDTOLoaded(DTO dto) throws RemoteException {
                        if(!emitter.isDisposed()) {
                            emitter.onSuccess(dto);
                        }
                    }
                });
            }
        });
    }

    @Override
    public void release() {
        isShuttingDownManually = true;
        isConnecting = false;
        isConnected = false;
        context.unbindService(connection);
    }

    private void establishConnection() {
        isConnecting = true;
        context.bindService(
                new Intent(context, RepositoryContainer.class),
                connection,
                Context.BIND_AUTO_CREATE
        );
    }

    private <T> void delaySubscription(SingleEmitter<T> emitter) throws InterruptedException {
        while(!isConnected && !emitter.isDisposed() && !isShuttingDownManually) {
            Thread.sleep(10);
        }
    }
}
