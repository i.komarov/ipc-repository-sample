package net.styleru.ikomarov.ipcrepository.client;

import android.app.Activity;
import android.os.Bundle;

import net.styleru.ikomarov.ipcrepository.dto.DTO;
import net.styleru.ikomarov.ipcrepository.repository.IRepository;
import net.styleru.ikomarov.ipcrepository.serivce.RepositoryProxy;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 30.05.17.
 */

public class ClientActivity extends Activity {

    private IRepository repository;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repository = new RepositoryProxy(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        repository.create(new DTO("1", "name_1"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        System.out::println,
                        Throwable::printStackTrace
                );
    }
}
