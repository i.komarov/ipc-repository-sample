// IRepositoryAdapter.aidl
package net.styleru.ikomarov.ipcrepository.repository;

// Declare any non-default types here with import statements
import net.styleru.ikomarov.ipcrepository.dto.DTO;
import net.styleru.ikomarov.ipcrepository.repository.IDTOGetCallback;
import net.styleru.ikomarov.ipcrepository.repository.IDTOCreateCallback;

interface IRepositoryAdapter {

    void create(in DTO dto, in IDTOCreateCallback callback);

    void get(in String id, in IDTOGetCallback callback);
}
