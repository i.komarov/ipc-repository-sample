// IDTOCreateCallback.aidl
package net.styleru.ikomarov.ipcrepository.repository;

// Declare any non-default types here with import statements
import net.styleru.ikomarov.ipcrepository.dto.DTO;

interface IDTOCreateCallback {

    void onDTOCreated(in DTO dto);
}
