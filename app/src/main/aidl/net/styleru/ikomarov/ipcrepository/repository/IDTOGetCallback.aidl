// IDTOGetCallback.aidl
package net.styleru.ikomarov.ipcrepository.repository;

// Declare any non-default types here with import statements
import net.styleru.ikomarov.ipcrepository.dto.DTO;

interface IDTOGetCallback {

    void onDTOLoaded(in DTO dto);
}
